use nom::character::complete::{alpha1, char, digit1};
use nom::multi::many0;
use nom::{bytes::complete::tag, sequence::preceded, IResult};

#[derive(Debug, Eq, PartialEq)]
enum Operator {
    Equal,
    NotEqual,
    GreaterThanEqual,
    LessThanEqual,
    GreaterThan,
    LessThan,
}

// we have no operator precedence!
#[derive(Debug, Eq, PartialEq)]
enum Expression {
    Number(String),
    FunctionApplication { name: String, args: Vec<Expression> },
    OpTerm(Box<Expression>, Vec<(Operator, Expression)>),
}

#[derive(Debug, Eq, PartialEq)]
struct Function<'a> {
    name: &'a str,
    expr: Expression,
}

fn parse_number(input: &str) -> IResult<&str, Expression> {
    let (input, digits) = digit1(input)?;
    Ok((input, Expression::Number(digits.into())))
}

// unary operators are just functions.
// infix operators are binary functions
//
// <EXPR> ::= <TERM> (<OPERATOR> <TERM>)*
// <TERM> ::= <Literal> | <FunctionApplication> | '(' <EXPR> ')'
// <FunctionApplication> ::= <FunctionName> <FunctionArg>*
// <ConstantFunction> ::= <FunctionName>
// <FunctionArg> ::= <Literal> | <ConstantFunction> | '(' <EXPR> ')'
//
fn parse_expr(input: &str) -> IResult<&str, Expression> {
    let (input, left_term) = parse_term(input)?;

    let (input, more_terms) = many0(parse_op_and_term)(input)?;

    if more_terms.is_empty() {
        Ok((input, left_term))
    } else {
        Ok((input, Expression::OpTerm(Box::new(left_term), more_terms)))
    }
}

fn parse_op_and_term(input: &str) -> IResult<&str, (Operator, Expression)> {
    let (input, _) = char(' ')(input)?;
    let (input, op) = parse_operator(input)?;
    let (input, _) = char(' ')(input)?;
    let (input, term) = parse_term(input)?;
    Ok((input, (op, term)))
}

fn parse_equal_operator(input: &str) -> IResult<&str, Operator> {
    let (input, _) = tag("=")(input)?;
    Ok((input, Operator::Equal))
}

fn parse_not_equal_operator(input: &str) -> IResult<&str, Operator> {
    let (input, _) = tag("/=")(input)?;
    Ok((input, Operator::NotEqual))
}

fn parse_greater_than_equal_operator(input: &str) -> IResult<&str, Operator> {
    let (input, _) = tag(">=")(input)?;
    Ok((input, Operator::GreaterThanEqual))
}

fn parse_less_than_equal_operator(input: &str) -> IResult<&str, Operator> {
    let (input, _) = tag("<=")(input)?;
    Ok((input, Operator::LessThanEqual))
}

fn parse_greater_than_operator(input: &str) -> IResult<&str, Operator> {
    let (input, _) = tag(">")(input)?;
    Ok((input, Operator::GreaterThan))
}

fn parse_less_than_operator(input: &str) -> IResult<&str, Operator> {
    let (input, _) = tag("<")(input)?;
    Ok((input, Operator::LessThan))
}

fn parse_operator(input: &str) -> IResult<&str, Operator> {
    parse_equal_operator(input)
        .or_else(|_| parse_not_equal_operator(input))
        .or_else(|_| parse_greater_than_equal_operator(input))
        .or_else(|_| parse_less_than_equal_operator(input))
        .or_else(|_| parse_greater_than_operator(input))
        .or_else(|_| parse_less_than_operator(input))
}

fn parse_literal(input: &str) -> IResult<&str, Expression> {
    parse_number(input)
}

// <TERM> ::= <Literal> | <FunctionApplication> | ( <EXPR> )
fn parse_term(input: &str) -> IResult<&str, Expression> {
    parse_literal(input)
        .or_else(|_| parse_function_application(input))
        .or_else(|_| parse_parenthesized_expr(input))
}

// <FunctionApplication> ::= <FunctionName> (' ' <FunctionArg>)*
fn parse_function_application(input: &str) -> IResult<&str, Expression> {
    let (input, name) = parse_function_name(input)?;
    let (input, args) = many0(preceded(char(' '), parse_function_arg))(input)?;
    Ok((
        input,
        Expression::FunctionApplication {
            name: name.into(),
            args,
        },
    ))
}

fn parse_function_name(input: &str) -> IResult<&str, &str> {
    alpha1(input)
}

// <FunctionArg> ::= <Literal> | <ConstantFunction> | '(' <EXPR> ')'
fn parse_function_arg(input: &str) -> IResult<&str, Expression> {
    parse_literal(input)
        .or_else(|_| parse_constant_function(input))
        .or_else(|_| parse_parenthesized_expr(input))
}

fn parse_parenthesized_expr(input: &str) -> IResult<&str, Expression> {
    let (input, _) = char('(')(input)?;
    let (input, expr) = parse_expr(input)?;
    let (input, _) = char(')')(input)?;
    Ok((input, expr))
}

fn parse_constant_function(input: &str) -> IResult<&str, Expression> {
    let (input, fname) = parse_function_name(input)?;
    Ok((
        input,
        Expression::FunctionApplication {
            name: fname.into(),
            args: vec![],
        },
    ))
}

fn parse_function(input: &str) -> IResult<&str, Function> {
    let (input, _) = tag("function")(input)?;
    let (input, _) = char(' ')(input)?;
    let (input, name) = alpha1(input)?;
    let (input, _) = char(' ')(input)?;
    let (input, _) = tag("is")(input)?;
    let (input, _) = char(' ')(input)?;
    let (input, expr) = parse_expr(input)?;
    Ok((input, Function { name, expr }))
}

#[test]
fn it_should_parse_two_terms() {
    let f1 = Expression::FunctionApplication {
        name: "f".into(),
        args: vec![Expression::Number("1".into())],
    };
    let f2 = Expression::FunctionApplication {
        name: "f".into(),
        args: vec![Expression::Number("2".into())],
    };

    assert_eq!(
        parse_expr("f 1 = f 2"),
        Ok((
            "",
            Expression::OpTerm(Box::new(f1), vec![(Operator::Equal, f2)])
        ))
    );
}

#[test]
fn it_should_parse_function_application() {
    let f1 = Expression::FunctionApplication {
        name: "f".into(),
        args: vec![Expression::Number("1".into())],
    };
    assert_eq!(parse_expr("f 1"), Ok(("", f1)));
}

#[test]
fn it_should_parse_function_application_with_parenthesized_subexpressions() {
    let f1 = Expression::FunctionApplication {
        name: "f".into(),
        args: vec![Expression::Number("1".into())],
    };
    assert_eq!(parse_expr("f (1)"), Ok(("", f1)));
}

#[test]
fn it_should_parse_multiple_terms() {
    let f1 = Expression::FunctionApplication {
        name: "f".into(),
        args: vec![Expression::Number("1".into())],
    };
    let f2 = Expression::FunctionApplication {
        name: "f".into(),
        args: vec![Expression::Number("2".into())],
    };
    let f3 = Expression::FunctionApplication {
        name: "f".into(),
        args: vec![Expression::Number("3".into())],
    };

    assert_eq!(
        parse_expr("f 1 = f 2 = f 3"),
        Ok((
            "",
            Expression::OpTerm(
                Box::new(f1),
                vec![(Operator::Equal, f2), (Operator::Equal, f3)]
            )
        ))
    );
}

#[test]
fn it_should_parse_parenthesized_terms() {
    let f1 = Expression::FunctionApplication {
        name: "f".into(),
        args: vec![Expression::Number("1".into())],
    };
    let f2 = Expression::FunctionApplication {
        name: "f".into(),
        args: vec![Expression::Number("2".into())],
    };
    let f3 = Expression::FunctionApplication {
        name: "f".into(),
        args: vec![Expression::Number("3".into())],
    };

    let f2f3 = Expression::OpTerm(Box::new(f2), vec![(Operator::Equal, f3)]);

    assert_eq!(
        parse_expr("f 1 = (f 2 = f 3)"),
        Ok((
            "",
            Expression::OpTerm(Box::new(f1), vec![(Operator::Equal, f2f3)])
        ))
    );
}

#[test]
fn it_should_parse_function() {
    assert_eq!(
        parse_function("function A is 123"),
        Ok((
            "",
            Function {
                name: "A",
                expr: Expression::Number("123".into())
            }
        ))
    );
    assert_eq!(
        parse_function("function A is var"),
        Ok((
            "",
            Function {
                name: "A",
                expr: Expression::FunctionApplication {
                    name: "var".into(),
                    args: vec![]
                }
            }
        ))
    );
    assert_eq!(
        parse_function("function A is add 1 (2) 3"),
        Ok((
            "",
            Function {
                name: "A",
                expr: Expression::FunctionApplication {
                    name: "add".into(),
                    args: vec![
                        Expression::Number("1".into()),
                        Expression::Number("2".into()),
                        Expression::Number("3".into()),
                    ]
                }
            }
        ))
    );
    assert_eq!(
        parse_function("function A is add 1 add 2 3 "),
        Ok((
            " ",
            Function {
                name: "A",
                expr: Expression::FunctionApplication {
                    name: "add".into(),
                    args: vec![
                        Expression::Number("1".into()),
                        Expression::FunctionApplication {
                            name: "add".into(),
                            args: vec![]
                        },
                        Expression::Number("2".into()),
                        Expression::Number("3".into()),
                    ]
                }
            }
        ))
    );
}

fn main() {}
